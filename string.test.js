const Replace = require('./string')

describe('String.prototype.replace', () => {
    test('Should replace all matches', () => {
        const expected = '...'
        const actual = Replace('aaa', 'a', '.')
        expect(actual).toBe(expected, 'Should replace all instances')
    })

    test('Should replace a single instance when prefix used', () => {
        const result = Replace('prefix-dialog.prefix-dialog-open', 'prefix-dialog', '.prefix-dialog-0')
        expect(result).toMatch(/.prefix-dialog-0.prefix-dialog-open/, 'Should replace a single instance')
    })

    test('Should reproduce the bug when prefix not used', () => {
        const result = Replace('dialog.dialog-open', 'dialog', '.dialog-0')

        // This is bad behavior
        expect(result).toMatch(/.dialog-0..dialog-0-open/, 'Should replace all instances')

        // This is good behavior
        expect(result).not.toMatch(/.dialog-0.dialog-open/, 'Should replace a single instance')
    })

    test('Should not blow up when regex is used', () => {
        expect( () => {
            Replace('aaa', /a/g, '.')
        }).not.toThrow()
    })
})