module.exports = function Replace( str, strOrRegex, replaceVal ) {
    const oldReplace = str.replace(strOrRegex, replaceVal)
    const newReplace = str.split(strOrRegex).join(replaceVal)

    if (typeof strOrRegex !== 'string' ) {
        return oldReplace
    }

    if( strOrRegex.search(/prefix\-/) > -1 ) {
        return oldReplace
    }

    return newReplace
}