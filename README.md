[![pipeline status](https://gitlab.com/jbydeley/StringRefactorTests/badges/master/pipeline.svg)](https://gitlab.com/jbydeley/StringRefactorTests/commits/master)

# StringRefactorTests

Just some tests around a monkey patching issue. For more information check out the [blog post](https://jbydeley.gitlab.io/posts/monkey-patching/). 

## Getting Started

I don't know why you would but sure...

1. `git clone git@gitlab.com:jbydeley/StringRefactorTests.git`
2. `yarn install` or `npm install`
3. `yarn run test` or `npm run test`
4. (Optional) `yarn run watch` or `npm run watch` to watch the file